# stellar-escrow-smart-contracts
React, NodeJS application with sample escrow account creation using Stellar Smart Contract example

https://tecsynt-stellar-escrow.herokuapp.com/

Working example of the 2-Party Multisignature Escrow Account with Time Lock & Recovery

https://www.stellar.org/developers/guides/walkthroughs/stellar-smart-contracts.html#2-party-multisignature-escrow-account-with-time-lock-recovery
